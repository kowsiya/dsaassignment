import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class MyPalindrameChecker {
	private static Stack<Character> charStack;
	private static Queue<Character> charQueue;

	Stack<String> nameStack = new Stack<>();

	public static boolean checkpali(String word) {
		charStack = new Stack();
		charQueue = new LinkedList<>();

		// TODO Auto-generated method stub
		char charword[] = word.toUpperCase().toCharArray();

		for (char c : charword) {
			if (c != ' ') {
				charQueue.add(c);
				charStack.add(c);
			}
		}
		while (!charStack.isEmpty()) {
			if (charStack.pop() != charQueue.poll()) {
				return false;
			}
		}
		return true;
	}
}
